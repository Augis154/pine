#pragma once

#include "Device.hpp"
// #include "Swapchain.hpp"

// #include <cstdint>

#include <memory>
#include <string>
#include <vector>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

struct PipelineConfigInfo {
  PipelineConfigInfo() = default;
  PipelineConfigInfo(const PipelineConfigInfo &) = delete;
  PipelineConfigInfo &operator=(const PipelineConfigInfo &) = delete;

  std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
  std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

  VkPipelineViewportStateCreateInfo viewportState;
  VkPipelineInputAssemblyStateCreateInfo inputAssembly;
  VkPipelineRasterizationStateCreateInfo rasterizer;
  VkPipelineMultisampleStateCreateInfo multisampler;
  VkPipelineColorBlendAttachmentState colorBlendAttachment;
  VkPipelineColorBlendStateCreateInfo colorBlending;
  VkPipelineDepthStencilStateCreateInfo depthStencil;
  std::vector<VkDynamicState> dynamicStates;
  VkPipelineDynamicStateCreateInfo dynamicState;

  VkPipelineLayout pipelineLayout = nullptr;
  VkRenderPass renderPass = nullptr;
  uint32_t subpass = 0;

  VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;
};

class Pipeline {
public:
  ~Pipeline();

  static std::unique_ptr<Pipeline> create(Device &device,
                                          const PipelineConfigInfo &configInfo,
                                          std::string vertShaderFile,
                                          std::string fragShaderFile);

  Pipeline(const Pipeline &) = delete;
  Pipeline &operator=(const Pipeline &) = delete;

  void cleanup();

  void bind(VkCommandBuffer commandBuffer);

  static void defaultPipelineConfigInfo(PipelineConfigInfo &configInfo);
  static void enableAlphaBlending(PipelineConfigInfo &configInfo);
  static void setMsaaSampleCount(PipelineConfigInfo &configInfo,
                                 VkSampleCountFlagBits msaaSamples);

  VkPipeline getPipeline() { return pipeline; }

private:
  Pipeline(Device &device);

  static std::vector<char> readFile(const std::string filename);

  static VkShaderModule createShaderModule(Device &device,
                                           const std::vector<char> &code);

  inline static VkPipeline pipeline;

  Device &device;
};