#pragma once

#include "Allocator.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <vector>

class Model {
public:
  Model(Allocator &allocator);

  void createModel(std::string filePath);
  void cleanup();

  void bind(VkCommandBuffer commandBuffer);
  void draw(VkCommandBuffer commandBuffer);

  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 color;
    glm::vec2 texture;

    static std::vector<VkVertexInputBindingDescription>
    getBindingDescriptions();

    static std::vector<VkVertexInputAttributeDescription>
    getAttributeDescriptions();

    bool operator==(const Vertex &other) const {
      return position == other.position && color == other.color &&
             texture == other.texture;
    }
  };

private:
  void loadModel(std::string modelFile);
  void createVertexBuffer();
  void createIndexBuffer();

  VkBuffer vertexBuffer;
  VmaAllocation vertexBufferAllocation;

  VkBuffer indexBuffer;
  VmaAllocation indexBufferAllocation;

  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;

  Allocator &allocator;
};