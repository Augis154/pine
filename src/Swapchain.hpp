#pragma once

#include "Device.hpp"
#include "Window.hpp"

#include <cstdint>
#include <vulkan/vulkan.h>

#include <vector>
#include <vulkan/vulkan_core.h>

#include "Allocator.hpp"

class Swapchain {
public:
  Swapchain(Window &window, Device &device, Allocator &allocator);

  void init();

  void transitionImageLayout(VkImage image, VkFormat format,
                             VkImageLayout oldLayout, VkImageLayout newLayout,
                             uint32_t mipLevels);

  VkFormat getImageFormat() { return swapChainImageFormat; }
  VkFormat findDepthFormat();

  VkFramebuffer getFramebuffer(int index) {
    return swapChainFramebuffers[index];
  }

  VkExtent2D getExtent() { return swapChainExtent; }
  uint32_t getExtentWidth() { return swapChainExtent.width; }
  uint32_t getExtentHeight() { return swapChainExtent.height; }

  VkSwapchainKHR getSwapChain() { return swapChain; }
  VkRenderPass getRenderPass() { return renderPass; }

  void createSwapChain();
  void createImageViews();
  void cleanupSwapChain();
  void recreateSwapChain();

private:
  VkSurfaceFormatKHR chooseSwapSurfaceFormat(
      const std::vector<VkSurfaceFormatKHR> &availableFormats);

  VkPresentModeKHR chooseSwapPresentMode(
      const std::vector<VkPresentModeKHR> &availablePresentModes);

  VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);

  VkImageView createImageView(VkImage image, VkFormat format,
                              VkImageAspectFlags aspectFlags,
                              uint32_t mipLevels);

  void createRenderPass();

  void createColorResources();
  void createDepthResources();
  void createFramebuffers();

  VkFormat findSupportedFormat(const std::vector<VkFormat> &candidates,
                               VkImageTiling tiling,
                               VkFormatFeatureFlags features);
  bool hasStencilComponent(VkFormat format);

  VkSwapchainKHR swapChain;
  VkExtent2D swapChainExtent;

  std::vector<VkImage> swapChainImages;
  VkFormat swapChainImageFormat;
  std::vector<VkImageView> swapChainImageViews;

  std::vector<VkFramebuffer> swapChainFramebuffers;

  VkImage colorImage;
  VmaAllocation colorImageAllocation;
  VkImageView colorImageView;

  VkImage depthImage;
  VmaAllocation depthImageAllocation;
  VkImageView depthImageView;

  VkRenderPass renderPass;

  Window &window;
  Device &device;
  Allocator &allocator;
};