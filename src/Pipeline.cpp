#include "Pipeline.hpp"

#include "Model.hpp"
// #include "Swapchain.hpp"

#include <algorithm>
#include <fstream>
#include <memory>
#include <stdexcept>

#include <vulkan/vulkan_core.h>

Pipeline::Pipeline(Device &device) : device{device} {}

Pipeline::~Pipeline() { cleanup(); }

void Pipeline::cleanup() {
  vkDestroyPipeline(device.getDevice(), pipeline, nullptr);
}

std::unique_ptr<Pipeline> Pipeline::create(Device &device,
                                           const PipelineConfigInfo &configInfo,
                                           std::string vertShaderFile,
                                           std::string fragShaderFile) {
  auto vertShaderCode = readFile(vertShaderFile);
  auto fragShaderCode = readFile(fragShaderFile);

  VkShaderModule vertShaderModule = createShaderModule(device, vertShaderCode);
  VkShaderModule fragShaderModule = createShaderModule(device, fragShaderCode);

  // Vertex shader
  VkPipelineShaderStageCreateInfo vertShaderStageInfo{};
  vertShaderStageInfo.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
  vertShaderStageInfo.module = vertShaderModule;
  vertShaderStageInfo.pName = "main";

  // Fragment shader
  VkPipelineShaderStageCreateInfo fragShaderStageInfo{};
  fragShaderStageInfo.sType =
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  fragShaderStageInfo.module = fragShaderModule;
  fragShaderStageInfo.pName = "main";

  VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo,
                                                    fragShaderStageInfo};

  auto bindingDescriptions = configInfo.bindingDescriptions;
  auto attributeDescriptions = configInfo.attributeDescriptions;

  // Vertex input state
  VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  vertexInputInfo.vertexAttributeDescriptionCount =
      static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.vertexBindingDescriptionCount =
      static_cast<uint32_t>(bindingDescriptions.size());
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();

  VkGraphicsPipelineCreateInfo pipelineInfo{};
  pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipelineInfo.stageCount = 2;
  pipelineInfo.pStages = shaderStages;

  pipelineInfo.pVertexInputState = &vertexInputInfo;
  pipelineInfo.pInputAssemblyState = &configInfo.inputAssembly;
  pipelineInfo.pViewportState = &configInfo.viewportState;
  pipelineInfo.pRasterizationState = &configInfo.rasterizer;
  pipelineInfo.pMultisampleState = &configInfo.multisampler;
  pipelineInfo.pDepthStencilState = nullptr; // Optional
  pipelineInfo.pColorBlendState = &configInfo.colorBlending;
  pipelineInfo.pDynamicState = &configInfo.dynamicState;
  pipelineInfo.pDepthStencilState = &configInfo.depthStencil;

  pipelineInfo.layout = configInfo.pipelineLayout;
  pipelineInfo.renderPass = configInfo.renderPass;
  pipelineInfo.subpass = configInfo.subpass;

  pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
  pipelineInfo.basePipelineIndex = -1;              // Optional

  VkResult result = vkCreateGraphicsPipelines(
      device.getDevice(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline);
  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create graphics pipeline!");
  }

  vkDestroyShaderModule(device.getDevice(), fragShaderModule, nullptr);
  vkDestroyShaderModule(device.getDevice(), vertShaderModule, nullptr);

  return std::unique_ptr<Pipeline>(new Pipeline(device));
}

void Pipeline::bind(VkCommandBuffer commandBuffer) {
  vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
}

VkShaderModule Pipeline::createShaderModule(Device &device,
                                            const std::vector<char> &code) {
  VkShaderModuleCreateInfo createInfo{};
  createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  createInfo.codeSize = code.size();
  createInfo.pCode = reinterpret_cast<const uint32_t *>(code.data());

  VkShaderModule shaderModule;
  VkResult result = vkCreateShaderModule(device.getDevice(), &createInfo,
                                         nullptr, &shaderModule);
  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create shader module!");
  }

  return shaderModule;
}

std::vector<char> Pipeline::readFile(const std::string filename) {
  std::ifstream file(filename, std::ios::ate | std::ios::binary);

  if (!file.is_open()) {
    throw std::runtime_error("Failed to open file! " + filename);
  }

  size_t fileSize = (size_t)file.tellg();
  std::vector<char> buffer(fileSize);

  //   std::cout << filename << " file size: " << fileSize << std::endl;

  file.seekg(0);
  file.read(buffer.data(), fileSize);

  file.close();

  return buffer;
}

void Pipeline::defaultPipelineConfigInfo(PipelineConfigInfo &configInfo) {
  // Input assembly state
  configInfo.inputAssembly.sType =
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  configInfo.inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  configInfo.inputAssembly.primitiveRestartEnable = VK_FALSE;

  configInfo.dynamicStates = {VK_DYNAMIC_STATE_VIEWPORT,
                              VK_DYNAMIC_STATE_SCISSOR};

  // Dynamic states
  configInfo.dynamicState.sType =
      VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  configInfo.dynamicState.dynamicStateCount =
      static_cast<uint32_t>(configInfo.dynamicStates.size());
  configInfo.dynamicState.pDynamicStates = configInfo.dynamicStates.data();

  configInfo.viewportState.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  configInfo.viewportState.viewportCount = 1;
  configInfo.viewportState.scissorCount = 1;

  // Rasterizer state
  configInfo.rasterizer.sType =
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  configInfo.rasterizer.depthClampEnable = VK_FALSE;
  configInfo.rasterizer.rasterizerDiscardEnable = VK_FALSE;
  configInfo.rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
  configInfo.rasterizer.lineWidth = 1.0f;
  configInfo.rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
  configInfo.rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
  configInfo.rasterizer.depthBiasEnable = VK_FALSE;
  configInfo.rasterizer.depthBiasConstantFactor = 0.0f; // Optional
  configInfo.rasterizer.depthBiasClamp = 0.0f;          // Optional
  configInfo.rasterizer.depthBiasSlopeFactor = 0.0f;    // Optional

  // Multisampling
  configInfo.multisampler.sType =
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  configInfo.multisampler.sampleShadingEnable = VK_FALSE;
  configInfo.multisampler.rasterizationSamples = configInfo.msaaSamples;
  configInfo.multisampler.minSampleShading = 1.0f;          // Optional
  configInfo.multisampler.pSampleMask = nullptr;            // Optional
  configInfo.multisampler.alphaToCoverageEnable = VK_FALSE; // Optional
  configInfo.multisampler.alphaToOneEnable = VK_FALSE;      // Optional

  // Color blending
  configInfo.colorBlendAttachment.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
  configInfo.colorBlendAttachment.blendEnable = VK_FALSE;
  configInfo.colorBlendAttachment.srcColorBlendFactor =
      VK_BLEND_FACTOR_ONE; // Optional
  configInfo.colorBlendAttachment.dstColorBlendFactor =
      VK_BLEND_FACTOR_ZERO;                                       // Optional
  configInfo.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
  configInfo.colorBlendAttachment.srcAlphaBlendFactor =
      VK_BLEND_FACTOR_ONE; // Optional
  configInfo.colorBlendAttachment.dstAlphaBlendFactor =
      VK_BLEND_FACTOR_ZERO;                                       // Optional
  configInfo.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

  // colorBlendAttachment.blendEnable = VK_TRUE;
  // colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
  // colorBlendAttachment.dstColorBlendFactor =
  //     VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  // colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
  // colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  // colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  // colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

  configInfo.colorBlending.sType =
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  configInfo.colorBlending.logicOpEnable = VK_FALSE;
  configInfo.colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
  configInfo.colorBlending.attachmentCount = 1;
  configInfo.colorBlending.pAttachments = &configInfo.colorBlendAttachment;
  configInfo.colorBlending.blendConstants[0] = 0.0f; // Optional
  configInfo.colorBlending.blendConstants[1] = 0.0f; // Optional
  configInfo.colorBlending.blendConstants[2] = 0.0f; // Optional
  configInfo.colorBlending.blendConstants[3] = 0.0f; // Optional

  configInfo.depthStencil.sType =
      VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  configInfo.depthStencil.depthTestEnable = VK_TRUE;
  configInfo.depthStencil.depthWriteEnable = VK_TRUE;
  configInfo.depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;

  configInfo.depthStencil.depthBoundsTestEnable = VK_FALSE;
  configInfo.depthStencil.minDepthBounds = 0.0f; // Optional
  configInfo.depthStencil.maxDepthBounds = 1.0f; // Optional

  configInfo.depthStencil.stencilTestEnable = VK_FALSE;
  configInfo.depthStencil.front = {}; // Optional
  configInfo.depthStencil.back = {};  // Optional

  configInfo.bindingDescriptions = Model::Vertex::getBindingDescriptions();
  configInfo.attributeDescriptions = Model::Vertex::getAttributeDescriptions();
}

void Pipeline::enableAlphaBlending(PipelineConfigInfo &configInfo) {
  configInfo.colorBlendAttachment.blendEnable = VK_TRUE;

  configInfo.colorBlendAttachment.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
  configInfo.colorBlendAttachment.srcColorBlendFactor =
      VK_BLEND_FACTOR_SRC_ALPHA;
  configInfo.colorBlendAttachment.dstColorBlendFactor =
      VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  configInfo.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
  configInfo.colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
  configInfo.colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
  configInfo.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
}

void Pipeline::setMsaaSampleCount(PipelineConfigInfo &configInfo,
                                  VkSampleCountFlagBits msaaSamples) {
  configInfo.msaaSamples = msaaSamples;
  configInfo.multisampler.rasterizationSamples = msaaSamples;
}