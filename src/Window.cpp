#include "Window.hpp"
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_stdinc.h>
#include <iostream>
#include <stdexcept>

Window::Window(std::string title) {
  this->title = title;
  //   init();
}

void Window::init() {
  int ret = SDL_InitSubSystem(SDL_INIT_VIDEO);
  if (ret != 0) {
    // fprintf(stderr, "SDL Init Error: %s\n", SDL_GetError());
    std::cerr << "SDL Init Error: " << SDL_GetError() << std::endl;
    throw std::runtime_error("Failed to initialize SDL");
    return;
  }

  window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, width, height,
                            SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI |
                                SDL_WINDOW_RESIZABLE);

  if (window == NULL) {
    // fprintf(stderr, "SDL Create Window Error: %s\n", SDL_GetError());
    std::cerr << "SDL Create Window Error: " << SDL_GetError() << std::endl;
    throw std::runtime_error("Failed to create SDL window");
    return;
  }
}

void Window::cleanup() {
  SDL_DestroyWindow(window);
  SDL_Quit();
}

std::vector<const char *> Window::getRequiredExtensions() {
  uint32_t extensionCount = 0;

  // Gets SDL extentions
  SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, nullptr);
  std::vector<const char *> extensions(extensionCount);
  SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, extensions.data());

  return extensions;
}

void Window::createSurface(VkInstance instance, VkSurfaceKHR *surface) {
  SDL_bool ret = SDL_Vulkan_CreateSurface(window, instance, surface);
  if (ret != SDL_TRUE) {
    throw std::runtime_error("Failed to create window surface");
  }
}

void Window::handleEvent(const SDL_Event event) {
  switch (event.type) {
  case SDL_WINDOWEVENT:
    switch (event.window.event) {
    case SDL_WINDOWEVENT_RESIZED:
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      resizeCallback(event.window.data1, event.window.data2);
      break;

    case SDL_WINDOWEVENT_MINIMIZED:
      minimized = true;
      break;

    case SDL_WINDOWEVENT_RESTORED:
      minimized = false;
      break;

    case SDL_WINDOWEVENT_CLOSE:
      shouldClose = true;
      break;
    }
    break;

  case SDL_QUIT:
    shouldClose = true;
    break;
  }
}

void Window::resizeCallback(int width, int height) {
  this->resized = true;
  this->width = width;
  this->height = height;
}