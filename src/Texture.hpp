#pragma once

#include "Allocator.hpp"
#include "Device.hpp"
#include "Swapchain.hpp"

#include <vulkan/vulkan.h>

class Texture {
public:
  Texture(Device &device, Allocator &allocator, Swapchain &swapchain);
  void createTexture(std::string filePath);
  void cleanup();

  VkImageView getImageView() { return textureImageView; }
  VkSampler getSampler() { return textureSampler; }

private:
  void createTextureImage(std::string texturePath);
  void createTextureImageView();
  void createTextureSampler();
  VkImageView createImageView(VkImage image, VkFormat format,
                              VkImageAspectFlags aspectFlags,
                              uint32_t mipLevels);
  void generateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth,
                       int32_t texHeight, uint32_t mipLevels);

  uint32_t mipLevels;

  VkImage textureImage;
  VmaAllocation textureImageAllocation;

  VkImageView textureImageView;
  VkSampler textureSampler;

  Device &device;
  Allocator &allocator;
  Swapchain &swapchain;
};