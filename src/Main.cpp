#include "Allocator.hpp"
#include "Device.hpp"
#include "Model.hpp"
#include "Pipeline.hpp"
#include "Renderer.hpp"
#include "Swapchain.hpp"
#include "Texture.hpp"
#include "Window.hpp"

#include "Imgui.hpp"

#include <iostream>
#include <vulkan/vulkan_core.h>

class Pinera {
public:
  void run() {
    window.init();
    initVulkan();
    imgui.initImgui();
    // initImgui();
    mainLoop();
    cleanup();
  }

private:
  //   void handleSdlEvent(const SDL_Event event) {
  //     switch (event.type) {
  //     case SDL_WINDOWEVENT:
  //       switch (event.window.event) {
  //       case SDL_WINDOWEVENT_RESIZED:
  //       case SDL_WINDOWEVENT_SIZE_CHANGED:
  //         windowResizeCallback(event.window.data1, event.window.data2);
  //         break;

  //       case SDL_WINDOWEVENT_MINIMIZED:
  //         windowMinimized = true;
  //         break;

  //       case SDL_WINDOWEVENT_RESTORED:
  //         windowMinimized = false;
  //         break;

  //       case SDL_WINDOWEVENT_CLOSE:
  //         shouldClose = true;
  //         break;
  //       }
  //       break;

  //     case SDL_QUIT:
  //       shouldClose = true;
  //       break;
  //     }
  //   }

  void initVulkan() {
    device.init();

    allocator.init();

    swapchain.init();
    renderer.init();
  }

  void mainLoop() {
    SDL_Event event;

    // std::cout << "vk api: " << std::hex << VK_MAKE_VERSION(1, 2, 0)
    //           << std::endl;
    // std::cout << "vk api: " << std::hex << VK_API_VERSION_1_2 << std::endl;
    // std::cout << "vma api: " << std::hex << VMA_VULKAN_VERSION << std::endl;
    std::cout << "MSAA samples: " << device.msaaSamplesMax << std::endl;

    while (!window.shouldClose) {
      while (SDL_PollEvent(&event)) {
        imgui.handleEvent(event);
        window.handleEvent(event);
      }

      imgui.newFrame();
      imgui.msaa_window();
      // imgui commands
      ImGui::ShowDemoWindow();

      renderer.drawFrame();
    }
    vkDeviceWaitIdle(device.getDevice());
  }

  void cleanup() {

    imgui.cleanup();

    renderer.cleanup();

    allocator.destroy();

    window.cleanup();
  }

  Window window{"Halo"};

  Device device{window};

  Allocator allocator{device};

  Swapchain swapchain{window, device, allocator};

  Texture texture{device, allocator, swapchain};
  Model model{allocator};

  Imgui imgui{device, window, swapchain};

  Renderer renderer{window, device,  allocator, swapchain,
                    model,  texture, imgui};

  // Imgui
  //   VkDescriptorPool imguiPool;
};

int main() {
  Pinera app;

  try {
    app.run();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;

  return 0;
}

// #ifdef __WIN32__
// int WinMain() { main(); }
// #endif