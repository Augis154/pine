#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <cstdint>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

class Window {
public:
  Window(std::string title);

  void init();
  void cleanup();

  bool shouldClose = false;
  bool resized = false;
  bool minimized = false;

  std::vector<const char *> getRequiredExtensions();
  void createSurface(VkInstance instance, VkSurfaceKHR *surface);

  VkExtent2D getExtent() { return VkExtent2D{width, height}; }

  SDL_Window *getWindow() { return window; }

  void handleEvent(const SDL_Event event);
  void resizeCallback(int width, int height);

private:
  SDL_Window *window;

  uint32_t width = 1280;
  uint32_t height = 720;

  std::string title;
};