#include "Model.hpp"

#include <cstddef>
#include <unordered_map>
#include <vulkan/vulkan_core.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
// #include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include "../includes/tiny_obj_loader.h"

namespace std {
template <> struct hash<Model::Vertex> {
  size_t operator()(Model::Vertex const &vertex) const {
    return ((hash<glm::vec3>()(vertex.position) ^
             (hash<glm::vec3>()(vertex.color) << 1)) >>
            1) ^
           (hash<glm::vec2>()(vertex.texture) << 1);
  }
};
} // namespace std

Model::Model(Allocator &allocator) : allocator{allocator} {}

void Model::createModel(std::string filePath) {
  loadModel(filePath);
  createVertexBuffer();
  createIndexBuffer();
}

void Model::cleanup() {
  allocator.destroyBuffer(indexBuffer, indexBufferAllocation);
  allocator.destroyBuffer(vertexBuffer, vertexBufferAllocation);
}

void Model::loadModel(std::string modelFile) {
  tinyobj::attrib_t attrib;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;
  std::string warn, err;

  if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err,
                        modelFile.c_str())) {
    throw std::runtime_error(warn + err);
  }

  std::unordered_map<Vertex, uint32_t> uniqueVertices{};

  for (const auto &shape : shapes) {
    for (const auto &index : shape.mesh.indices) {
      Vertex vertex{};

      vertex.position = {attrib.vertices[3 * index.vertex_index + 0],
                         attrib.vertices[3 * index.vertex_index + 1],
                         attrib.vertices[3 * index.vertex_index + 2]};

      vertex.texture = {attrib.texcoords[2 * index.texcoord_index + 0],
                        1.0f - attrib.texcoords[2 * index.texcoord_index + 1]};

      vertex.color = {1.0f, 1.0f, 1.0f};

      if (uniqueVertices.count(vertex) == 0) {
        uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
        vertices.push_back(vertex);
      }

      indices.push_back(uniqueVertices[vertex]);
    }
  }
}

void Model::bind(VkCommandBuffer commandBuffer) {
  VkBuffer buffers[] = {vertexBuffer};
  VkDeviceSize offsets[] = {0};
  vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offsets);

  //   if (hasIndexBuffer) {
  vkCmdBindIndexBuffer(commandBuffer, indexBuffer, 0, VK_INDEX_TYPE_UINT32);
  //   }
}

void Model::draw(VkCommandBuffer commandBuffer) {
  //   if (hasIndexBuffer)
  vkCmdDrawIndexed(commandBuffer, static_cast<uint32_t>(indices.size()), 1, 0,
                   0, 0);
  //   else
  // vkCmdDraw(commandBuffer, vertexCount, 1, 0, 0);
}

void Model::createVertexBuffer() {
  size_t bufferSize = sizeof(vertices[0]) * vertices.size();

  VmaAllocationCreateInfo createInfo{};
  createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
  createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
  createInfo.flags = 0;

  VkResult result = allocator.createBuffer(
      bufferSize,
      VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
      createInfo, vertexBuffer, vertexBufferAllocation);

  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create vertex buffer!");
  }

  allocator.copyDataToBuffer(vertexBufferAllocation, vertexBuffer,
                             vertices.data(), bufferSize);
}

void Model::createIndexBuffer() {
  VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

  VmaAllocationCreateInfo createInfo{};
  createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
  createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
  createInfo.flags = 0;

  VkResult result = allocator.createBuffer(
      bufferSize,
      VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
      createInfo, indexBuffer, indexBufferAllocation);

  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create index buffer!");
  }

  allocator.copyDataToBuffer(indexBufferAllocation, indexBuffer, indices.data(),
                             bufferSize);
}

std::vector<VkVertexInputBindingDescription>
Model::Vertex::getBindingDescriptions() {
  std::vector<VkVertexInputBindingDescription> bindingDescriptions(1);
  bindingDescriptions[0].binding = 0;
  bindingDescriptions[0].stride = sizeof(Vertex);
  bindingDescriptions[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
  return bindingDescriptions;
}

std::vector<VkVertexInputAttributeDescription>
Model::Vertex::getAttributeDescriptions() {

  std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

  attributeDescriptions.push_back(
      {0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, position)});
  attributeDescriptions.push_back(
      {1, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, normal)});
  attributeDescriptions.push_back(
      {2, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex, color)});

  attributeDescriptions.push_back(
      {3, 0, VK_FORMAT_R32G32_SFLOAT, offsetof(Vertex, texture)});

  return attributeDescriptions;

  return attributeDescriptions;
}