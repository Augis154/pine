#pragma once

#include "Allocator.hpp"
#include "Device.hpp"
#include "Model.hpp"
#include "Pipeline.hpp"
#include "Swapchain.hpp"
#include "Texture.hpp"
#include "Window.hpp"

#include "Imgui.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

#include <memory>

const int MAX_FRAMES_IN_FLIGHT = 2;

class Renderer {
public:
  Renderer(Window &window, Device &device, Allocator &allocator,
           Swapchain &swapchain, Model &model, Texture &texture, Imgui &imgui);

  void init();
  void cleanup();

  void createDescriptorSetLayout();

  void drawFrame();

private:
  void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
  void createPipeline(VkRenderPass renderPass);

  void createCommandBuffers();
  void recordCommandBuffer(VkCommandBuffer commandBuffer, uint32_t imageIndex);
  void createSyncObjects();

  void createUniformBuffers();
  void updateUniformBuffer(uint32_t currentImage);

  void createDescriptorPool();
  //   void createDescriptorSetLayout();
  void createDescriptorSets();

  std::vector<VkCommandBuffer> commandBuffers;

  std::vector<VkSemaphore> imageAvailableSemaphores;
  std::vector<VkSemaphore> renderFinishedSemaphores;
  std::vector<VkFence> inFlightFences;

  uint32_t currentFrame = 0;

  std::vector<VkBuffer> uniformBuffers;
  std::vector<VmaAllocation> uniformBuffersAllocations;

  VkDescriptorPool descriptorPool;
  VkDescriptorSetLayout descriptorSetLayout;
  std::vector<VkDescriptorSet> descriptorSets;

  VkPipelineLayout pipelineLayout;

  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
  };

  const std::string MODEL_PATH = "../Models/viking_room.obj";
  const std::string TEXTURE_PATH = "../Textures/viking_room.png";

  Window &window;
  Device &device;
  Allocator &allocator;
  Swapchain &swapchain;

  std::unique_ptr<Pipeline> pipeline;

  Model &model;
  Texture &texture;

  Imgui &imgui;
};