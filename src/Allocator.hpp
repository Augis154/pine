#pragma once

#include "Device.hpp"

#include <vulkan/vulkan_core.h>

// #include <cstdio>

#include "../includes/vk_mem_alloc.h"

class Allocator {
public:
  Allocator(Device &device);

  void init();
  void destroy();

  VkResult createBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
                        VmaAllocationCreateInfo allocationCreateInfo,
                        VkBuffer &buffer, VmaAllocation &bufferAlloaction);

  VkResult createImage(uint32_t width, uint32_t height, uint32_t mipLevels,
                       VkSampleCountFlagBits numSamples, VkFormat format,
                       VkImageTiling tiling, VkImageUsageFlags usage,
                       VmaAllocationCreateInfo allocationCreateInfo,
                       VkImage &image, VmaAllocation &imageAllocation);

  void destroyBuffer(VkBuffer buffer, VmaAllocation allocation);

  void destroyImage(VkImage image, VmaAllocation allocation);

  void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
  void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width,
                         uint32_t height);

  //   void *getMappedData(VmaAllocation allocation);

  // Copies data to buffer
  void copyDataToBuffer(VmaAllocation allocation, VkBuffer buffer, void *data,
                        size_t dataSize);

  // Copies data to buffer
  void copyDataToImage(VmaAllocation allocation, VkImage image, uint32_t width,
                       uint32_t height, void *data, size_t dataSize);

  //   void *getMappedData(VmaAllocation allocation) {
  //     return allocation->GetMappedData();
  //   }

private:
  VmaAllocator allocator;

  Device &device;

  //   VkInstance &instance;
  //   VkPhysicalDevice &physicalDevice;
  //   VkDevice &device;
};