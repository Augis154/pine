#include "Device.hpp"
#include <cstddef>
#include <iostream>
#include <vulkan/vulkan_core.h>
#define VMA_IMPLEMENTATION

// #define VMA_VULKAN_VERSION 1000000 // Vulkan 1.2
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1

#include "Allocator.hpp"

#include <stdexcept>

Allocator::Allocator(Device &device) : device{device} {}

void Allocator::init() {
  VmaVulkanFunctions vulkanFunctions{};
  vulkanFunctions.vkGetInstanceProcAddr = &vkGetInstanceProcAddr;
  vulkanFunctions.vkGetDeviceProcAddr = &vkGetDeviceProcAddr;

  VmaAllocatorCreateInfo allocatorCreateInfo{};
  allocatorCreateInfo.vulkanApiVersion = VK_API_VERSION_1_0;
  allocatorCreateInfo.physicalDevice = device.getPhysicalDevice();
  allocatorCreateInfo.device = device.getDevice();
  allocatorCreateInfo.instance = device.getInstance();
  allocatorCreateInfo.pVulkanFunctions = &vulkanFunctions;

  VkResult result = vmaCreateAllocator(&allocatorCreateInfo, &allocator);

  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create memory allocator!");
  }
}

void Allocator::destroy() { vmaDestroyAllocator(allocator); }

VkResult Allocator::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
                                 VmaAllocationCreateInfo allocationCreateInfo,
                                 VkBuffer &buffer,
                                 VmaAllocation &bufferAlloaction) {
  VkBufferCreateInfo bufferInfo{};
  bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  bufferInfo.size = size;
  bufferInfo.usage = usage;
  bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  return vmaCreateBuffer(allocator, &bufferInfo, &allocationCreateInfo, &buffer,
                         &bufferAlloaction, nullptr);
}

VkResult
Allocator::createImage(uint32_t width, uint32_t height, uint32_t mipLevels,
                       VkSampleCountFlagBits numSamples, VkFormat format,
                       VkImageTiling tiling, VkImageUsageFlags usage,
                       VmaAllocationCreateInfo allocationCreateInfo,
                       VkImage &image, VmaAllocation &imageAllocation) {
  VkImageCreateInfo imageInfo{};
  imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  imageInfo.imageType = VK_IMAGE_TYPE_2D;
  imageInfo.extent.width = width;
  imageInfo.extent.height = height;
  imageInfo.extent.depth = 1;
  imageInfo.mipLevels = mipLevels;
  imageInfo.arrayLayers = 1;
  imageInfo.format = format;
  imageInfo.tiling = tiling;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = usage;
  imageInfo.samples = numSamples;
  imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  return vmaCreateImage(allocator, &imageInfo, &allocationCreateInfo, &image,
                        &imageAllocation, nullptr);
}

// void *Allocator::getMappedData(VmaAllocation allocation) {
//   //   if (allocation->)
//   return allocation->GetMappedData();
// }

void Allocator::copyDataToBuffer(VmaAllocation allocation, VkBuffer buffer,
                                 void *data, size_t dataSize) {
  VkMemoryPropertyFlags memPropFlags;
  vmaGetAllocationMemoryProperties(allocator, allocation, &memPropFlags);

  if (memPropFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
    // Allocation ended up in a mappable memory and is already mapped - write to
    // it directly.
    memcpy(allocation->GetMappedData(), data, dataSize);
  } else {
    // Allocation ended up in a non-mappable memory - need to transfer.

    VkBuffer stagingBuffer;
    VmaAllocation stagingBufferAllocation;

    VmaAllocationCreateInfo stagingBufferCreateInfo{};
    stagingBufferCreateInfo.usage = VMA_MEMORY_USAGE_AUTO;
    stagingBufferCreateInfo.flags =
        VMA_ALLOCATION_CREATE_MAPPED_BIT |
        VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    VkResult result = createBuffer(dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                   stagingBufferCreateInfo, stagingBuffer,
                                   stagingBufferAllocation);

    if (result != VK_SUCCESS) {
      throw std::runtime_error("Failed to create staging buffer!");
    }

    memcpy(stagingBufferAllocation->GetMappedData(), data, dataSize);
    vmaFlushAllocation(allocator, stagingBufferAllocation, 0, VK_WHOLE_SIZE);

    copyBuffer(stagingBuffer, buffer, dataSize);
    destroyBuffer(stagingBuffer, stagingBufferAllocation);
  }
}

void Allocator::copyDataToImage(VmaAllocation allocation, VkImage image,
                                uint32_t width, uint32_t height, void *data,
                                size_t dataSize) {
  VkMemoryPropertyFlags memPropFlags;
  vmaGetAllocationMemoryProperties(allocator, allocation, &memPropFlags);

  VkBuffer stagingBuffer;
  VmaAllocation stagingBufferAllocation;

  VmaAllocationCreateInfo stagingBufferCreateInfo{};
  stagingBufferCreateInfo.usage = VMA_MEMORY_USAGE_AUTO;
  stagingBufferCreateInfo.flags =
      VMA_ALLOCATION_CREATE_MAPPED_BIT |
      VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

  VkResult result = createBuffer(dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                 stagingBufferCreateInfo, stagingBuffer,
                                 stagingBufferAllocation);

  if (result != VK_SUCCESS) {
    throw std::runtime_error("Failed to create staging buffer!");
  }

  memcpy(stagingBufferAllocation->GetMappedData(), data, dataSize);
  vmaFlushAllocation(allocator, stagingBufferAllocation, 0, VK_WHOLE_SIZE);

  copyBufferToImage(stagingBuffer, image, width, height);
  destroyBuffer(stagingBuffer, stagingBufferAllocation);
}

void Allocator::destroyBuffer(VkBuffer buffer, VmaAllocation allocation) {
  vmaDestroyBuffer(allocator, buffer, allocation);
}

void Allocator::destroyImage(VkImage image, VmaAllocation allocation) {
  vmaDestroyImage(allocator, image, allocation);
}

void Allocator::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer,
                           VkDeviceSize size) {
  VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();

  VkBufferCopy copyRegion{};
  copyRegion.size = size;
  vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

  device.endSingleTimeCommands(commandBuffer);
}

void Allocator::copyBufferToImage(VkBuffer buffer, VkImage image,
                                  uint32_t width, uint32_t height) {
  VkCommandBuffer commandBuffer = device.beginSingleTimeCommands();

  VkBufferImageCopy region{};
  region.bufferOffset = 0;
  region.bufferRowLength = 0;
  region.bufferImageHeight = 0;

  region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel = 0;
  region.imageSubresource.baseArrayLayer = 0;
  region.imageSubresource.layerCount = 1;

  region.imageOffset = {0, 0, 0};
  region.imageExtent = {width, height, 1};

  vkCmdCopyBufferToImage(commandBuffer, buffer, image,
                         VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

  device.endSingleTimeCommands(commandBuffer);
}