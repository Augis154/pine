#pragma once

#include "../includes/imgui/imgui.h"
#include "../includes/imgui/imgui_impl_sdl2.h"
#include "../includes/imgui/imgui_impl_vulkan.h"
#include "Device.hpp"
#include "Swapchain.hpp"
#include "Window.hpp"
#include <vulkan/vulkan_core.h>

class Imgui {
public:
  Imgui(Device &device, Window &window, Swapchain &swapchain);
  void initImgui();
  void cleanup();

  void newFrame();
  void render() { ImGui::Render(); }
  void draw(VkCommandBuffer commandBuffer) {
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), commandBuffer);
  }

  void handleEvent(SDL_Event &event) { ImGui_ImplSDL2_ProcessEvent(&event); }
  void msaa_window();

private:
  VkDescriptorPool imguiPool;

  Device &device;
  Window &window;
  Swapchain &swapchain;
};
