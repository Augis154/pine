CFLAGS = -std=c++17 -O2
LDFLAGS = -lglfw -lvulkan -ldl -lpthread 
# -Wl,--verbose

TARGET = Pinera
# $(TARGET): $(vertObjFiles) $(fragObjFiles)
$(TARGET): *.cpp
	g++ $(CFLAGS) -o $(TARGET) *.cpp $(LDFLAGS)

run: $(TARGET)
	$(TARGET)

clean:
	rm -f VulkanTest