#!/bin/bash
mkdir -p build-mingw
cd build-mingw
x86_64-w64-mingw32-cmake -S ../ -B .

if [ -z "$MAKEFLAGS" ]
then
    export MAKEFLAGS=-j$(($(nproc)/2))
    # export MAKEFLAGS=-j$(nproc)
fi

make && make shaders && mangohud wine ./Pinera
cd ..